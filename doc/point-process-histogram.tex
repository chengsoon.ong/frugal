\documentclass{article} % For LaTeX2e
\usepackage{times}

\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{url}

\newcommand{\RR}{\mathbb{R}}
\newcommand{\Lcal}{\mathcal{L}}
\newcommand{\Popt}{\mathcal{P}^\mathrm{opt}}
\newcommand{\dx}{\mathrm{d}x}

\title{Frugal Histograms}
\author{Cheng Soon Ong\\
\texttt{chengsoon.ong@anu.edu.au}}
\date{23 February 2016}


%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}

\maketitle
\begin{abstract}
  We show that histograms can be modelled as
  an inhomogeneous Poisson point process. This enables us to derive
  adaptive histograms, where bin sizes are chosen to optimise resolution.
\end{abstract}

\section{Quantiles to Histograms}
\label{sec:histogram}

Computing a series of quantiles assumes a uniform distribution on the quantiles, but may be highly
non-uniform in the distribution of the values. Instead of considering the CDF, we could consider
an approximation to the PDF, also known as histograms~\cite{pearson1895conmte,haerdle04nonsm}.
Adaptive histograms~\cite{scargle13stuats} provide a dynamic programming solution to choosing
the bin size that is appropriate for each region of data (Figure~\ref{fig:bayesblocks}).
It would be nice to have a low memory (streaming) version of this.

\begin{figure}[htbp]
\label{fig:bayesblocks}
\includegraphics[width=0.7\textwidth]{figures/bayesblocks3}
\caption{Adaptive histogram, from
\url{https://jakevdp.github.io/blog/2012/09/12/dynamic-programming-in-python/}.}
\end{figure}

\subsection{Fitness function for histograms}

Consider a nonhomogeneous Poisson Process on the real line with intensity $\lambda(x)$. Let
\[
\Lambda = \int_a^b \lambda(x) \dx
\]
be the total intensity in the interval $[a,b]\in\RR$. Hence the probability of observing
$n$ events in the interval $[a,b]$ is Poisson distributed with mean $\Lambda$, that is
\[
P(\mathrm{\#events} \in[a,b] = n) = \exp(-\Lambda) \frac{\Lambda^n}{n!}.
\]
The density at point $x_i$ is given by
\[
\frac{\lambda(x_i)}{\Lambda}.
\]

The likelihood of observing an ordered sample $x_1, \ldots, x_n$
(of size $n$) in the interval $[a,b]$
is given by
\begin{align*}
  \Lcal(\lambda, x_1, \ldots, x_n) &= P(n)\cdot P(x_1, \ldots, x_n | n)\\
  &= P(n)\cdot \prod_{i=1}^n P(x_i | n)\\
  &= \exp(-\Lambda) \frac{\Lambda^n}{n!} \prod_{i=1}^n \frac{\lambda(x_i)}{\Lambda}\\
  &= \exp(-\Lambda) \frac{1}{n!}\prod_{i=1}^n \lambda(x_i).
\end{align*}
Since the identity of the samples are not used, and we discard the order in which the samples
are received, there are $n!$ possible sequences of inputs which result in the same set of
examples $\{x_1, \ldots, x_n\}$. Hence the likelihood is
\[
  \Lcal(\lambda, x_1, \ldots, x_n) = \exp(-\Lambda) \prod_{i=1}^n \lambda(x_i).
\]
The negative log likelihood is given by
\begin{align*}
  -\log \Lcal(\lambda, x_1, \ldots, x_n) &= \Lambda - \sum_{i=1}^n \log \lambda(x_i)\\
  &= \int_a^b \lambda(x) \dx - \sum_{i=1}^n \log \lambda(x_i).
\end{align*}

\subsection{Piecewise constant intensity}

The above derivation is for a general non-homogeneous Poisson process. We now focus
on a particular bin of a histogram. Within the $k^\mathrm{th}$ bin,
the intensity is constant $\lambda$, and
we assume that the bin spans the interval $[a,b]$ of length $T_k$ and contains $N_k$ events.
Substituting the length and number of events into the negative log likelihood, we get
\[
-\log \Lcal(\lambda) = \lambda T_k - N_k \log \lambda
\]
Taking the derivative with respect to $\lambda$ and setting it to zero, we
get the maximum likelihood estimator
\begin{align*}
  \frac{\partial}{\partial \lambda} \left(-\log \Lcal(\lambda)\right)
  &= T_k - \frac{N_k}{\lambda} := 0\\
  &\Rightarrow \hat{\lambda} = \frac{N_k}{T_k}
\end{align*}
Hence the value of the log likelihood for the $k^\mathrm{th}$ bin is
\begin{align*}
  \log \Lcal(\hat{\lambda}) = - N_k + N_k \log \frac{N_k}{T_k}\\
  \log \Lcal(\hat{\lambda}) + N_k = N_k \left( \log N_k - \log T_k \right)
\end{align*}
as is given by \cite{scargle13stuats} as a fitness function for random events.
The total log likelihood for all bins is given by the sum of the log likelihoods of all $k$ bins.

\subsubsection{Dynamic Programming for Adaptive Histograms}

Consider a dataset with $N$ samples, indexed by $n$, that is
$\{x_1, \cdots, x_n, \cdots, x_N\}$ where $x_n\in\RR$. For convenience, we assume that
the samples are already sorted in ascending order, $x_1 < \cdots < x_n < \cdots < x_N$.
We would like to build a histogram with $K$ bins, which partitions the interval $[x_1, x_N]$
into $K$ pieces.
We assume that the bin edges are placed at the midpoint between two examples,
therefore there are $N-1$ possible locations to place the $K$ bin edges. Even though it
really lies between two examples, we denote the index $n^\mathrm{th}$ edge using $n$.
Naively, this seems like an exponential search space of ${N \choose K}$ possibilities,
but there exists a dynamic programming solution~\cite{scargle13stuats}.

The base case is straightforward, when $n=1$ there is only one bin, which is optimal.
Consider the fitness function if we choose to place an edge at the $r^\mathrm{th}$ example,
expressed in terms of the
$k^\mathrm{th}$ bin with $N_k$ samples and width $T_k$,
\[
  F(\mathcal{P}) = \sum_{k=1}^{K} N_k(\log N_k - \log T_k),
\]
where $\mathcal{P}$ is a partition.
Let $\Popt(n)$ be the optimal partition of the first $n$ cells. The value of the fitness function
for the optimal partition $F(\Popt(n))$ is stored for each $n$.
Assume we have completed
step $r$, identifying the optimal partition $\Popt(r)$.
To obtain $\Popt(r+1)$,
we consider the last block that starts at $r'$ and ends at $r+1$. The fitness of this block
is given by
\[
  f(r',r) = (r - r')(\log (r - r') - \log (x_{r} - x_{r'})).
\]
Consider the set of all partitions where the last block starts at $r'$. The only partition
in this set which could possibly be optimal is the one that consists of $\Popt(r'-1)$
followed by the current block. The fitness of this partition would be
\[
  F(\Popt(r'-1)) + f(r',r).
\]
Since we already store the value of $F(\Popt(n))$ for previous $n$, we can compute the overall
fitness by computing the single block fitness $f(n, r)$ for $n=1,\cdots, r$ and adding it to
the array of values $F(\Popt(r))$. The value of $r'$ that yields the optimal partition
$\Popt(r+1)$ can be found by maximising the overall fitness.

At the end of the forward pass, when $r=N$, it only remains to find the locations of the
bin edges. To do so efficiently, during the iteration above, we additionally store a back
pointer at every example that contains the index that maximises the overall fitness in
the previous paragraph. Using this array of back pointers, we start at the value $N$, and follow
the indices backwards.

\subsubsection{How high are the bins?}

An illustration of the resulting histogram is shown in Figure~\ref{fig:histogram-demo}.
While \cite{scargle13stuats} carefully describes how to find the partitions, less attention
was spent on identifying the actual height of the bin. In the implementation by Jake Vanderplas
in astropy and astroML, the number of examples in the bin $N_k$ was used, as is typically done
for standard histograms with uniform bin widths. However, the intensity of the Poisson process
given by $\frac{N_k}{T_k}$ more closely reflects the true density.

\begin{figure}[htbp]
  \includegraphics[width=\textwidth]{figures/adapt_histogram_gauss_mixture}
  \caption{The piecewise constant intensity, the log likelihood,
    the histogram with adaptive bin sizes, and the normal histogram with equal
    bin sizes. The true density is a mixture of a Gaussian distribution $\mathcal{N}(10,2)$ and
    a reversed exponential distribution.
  }
  \label{fig:histogram-demo}
\end{figure}

\subsection{Piecewise linear intensity}

Instead of a piecewise constant approximation, we consider the case when a particular bin of
the histogram is modelled by a linear function. For an interval $[a,b]$, the linear function is
defined by the values of the intensity at the bin edges $[\lambda(a), \lambda(b)]$. Hence
the intensity is given by
\[
  \lambda(x) = \frac{\lambda(b)-\lambda(a)}{b-a} (x-a) + \lambda(a)
\]
for $x\in[a,b]$. Substituting this into the definition of the negative log likelihood of the
Poisson process
\[
  -\log \Lcal(\lambda(x)) = \frac{1}{2}(\lambda(b)-\lambda(a))(b-a) -\sum_{i=1}^n blah
\]
The first term is obtained by observing that
\begin{align*}
  \int_a^b x-a \dx &= \left. \frac{1}{2} x^2 - ax \right|_a^b\\
  &= \frac{1}{2}b^2 - ab - \frac{1}{2} a^2 + a^2\\
  &= \frac{1}{2}(b-a)^2
\end{align*}
and
\[
  \int_a^b \lambda(a) \dx = \lambda(a)(b-a).
\]
Alternatively, one can use basic geometry by considering the sum of the rectangle and the
triangle formed by the linear function.



\newpage
\section{Ideas that did not work}


\subsection{Directly optimising the area}
It would be interesting to derive a continuous optimisation version of this.
Consider $f(x)$ to be a density, and hence
\[
  \int_{-\infty}^{\infty} f(x) \dx = 1.
\]
A histogram can be considered to be the Riemann sum, and the adaptive bin size version
just means that the subintervals of the Riemann sum are not uniformly large.

To illustrate the intuition, consider the case when we only have two partitions,
and the density is compact with support $[0,1]$. In this case, the adaptive histogram
only has to choose one value $x\in(0,1)$ such that the Riemann sum best approximates
the integral. For a particular choice of $x$ the score consists of two areas,
the first with the range $[0,x]$ and the second $[x,1]$. The Riemann sum is
\[
  S(x) = xf\left(\frac{1}{2}x\right) + (1-x)f\left(\frac{1}{2}(x+1)\right)
\]
where we have chosen to use the midpoint quadrature rule for the
function value $f(x)$~\cite{haber67midqf,haerdle04nonsm}.

We could use the least squares error to measure the difference in our area estimate
using the two blocks compared to the true integral (with value 1). Then the best value
$x$ for partitioning into two blocks can be found by solving the following optimisation
problem.
\[
  \min_{x\in(0,1)} \frac{1}{2}\left( {S(x)-1} \right)^2
\]

In general, we would partition into $K+1$ blocks, by choosing $K$ points to partition
the interval $[0,1]$.
\[
  S(x_1,\ldots,x_K) = \sum_{k=0}^K (x_{k+1}-x_k) f\left(\frac{1}{2}(x_{k+1}+x_k)\right)
\]
subject to the constraint that $0=x_0<x_1<x_2<\cdots<x_K<x_{K+1}=1$.

\begin{itemize}
\item The constraints are similar to isotonic regression
\item Isotonic regression assumes that the function is monotone. $f(x)$ is not monotone,
but the CDF is monotone.
\end{itemize}

\subsection{Naive approach does not work}
To estimate the value of $f(x)$ from finite data, we use the same procedure as in histograms:
divide the number of items in a bin with the total number of items, and divide also by the
bin width. The first operation provides the relative frequency which is the sample analogue
of probability. The second operation ensures that the area under the histogram sums to 1.
Unfortunately, the bin width $x_{k+1}-x_k$ cancels, leaving us with no direct value of $x$
to optimise over.

\subsection{Objection from Chris}
Here is some counter argument about $ S $:
Denote the CDF of $ f(x) $ by $ F(x) = \int_{-\infty}^x f(y) dy $. Then
for any choice of ordered $ x_1<x_2<\cdots<x_K$, the function $ S(x_1,\ldots,x_K) $ is \emph{exactly}
\[
  S(x_1,\ldots,x_K) = \sum_{k=0}^K \left( F(x_{k+1}) - F(x_k) \right)
                    = F(x_{K+1}) - F(x_0)
                    = F(1) - F(0) = \text{const}.
\]
because $ F(x_{k+1}) - F(x_k) $ represents the \emph{exact} area under
the density $f(x) $ for $ x_k \le x \le x_{k+1} $.

\subsection{We really want to use the pointwise estimate}

Consider the minimum integrated squared error~\cite{haerdle04nonsm}.


\subsubsection*{Acknowledgments}

\begin{itemize}
  \item Aditya Menon for discussions on isotonic regression
  \item Young Lee for background on Poisson processes
  \item Lexing Xie for background on dynamic programming
\end{itemize}
\subsubsection*{References}


\bibliographystyle{alpha}
\bibliography{sgd}


\end{document}
