\documentclass{article} % For LaTeX2e
\usepackage{times}

\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{minted}
\usepackage{hyperref}
\usepackage{url}

\newcommand{\RR}{\mathbb{R}}
\newcommand{\Lcal}{\mathcal{L}}
\newcommand{\Popt}{\mathcal{P}^\mathrm{opt}}
\newcommand{\dx}{\mathrm{d}x}

\title{Frugal Histograms}
\author{Cheng Soon Ong\\
\texttt{chengsoon.ong@anu.edu.au}}
\date{23 February 2016}


%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}

\maketitle
\begin{abstract}
  We propose a low memory method for estimating the empirical
  cumulative distribution function from streaming data. The main
  insight is that we can apply stochastic subgradient methods for
  quantile regression.
\end{abstract}

\section{Background}
\label{sec:background}

\subsection{Frugal streaming}

\begin{itemize}
  \item Survey data streams~\cite{gilbert07anadsc}.
  \item Survey quantiles~\cite{greenwald15quaehs}.
  \item Industrial use \url{https://metamarkets.com/2013/histograms/}
  \item Majority vote with one memory~\cite{boyer91mjrty}.
  \item Streaming mean and moments~\cite{pebay08forrop}.
  \item One memory and two memory quantiles~\cite{ma13fruseq}.
  \item Heuristic algorithm, P$^2$~\cite{jain85p2alg}.
  \item Distribution learning~\cite{benhaim10strpdt}.
\end{itemize}


\subsection{Quantile Regression}

\begin{itemize}
  \item Generalisation error bound pinball loss \url{http://arxiv.org/pdf/1102.2101.pdf}
  \item Generalisation error bound stochastic gradient descent
   \url{http://arxiv.org/pdf/1509.01240.pdf}
  \item Pinball loss~\cite{koenker78regq}.
\end{itemize}

We consider the difference between the label $y$ and our estimate
$f(x)$. Let $t := y - f(x)$.

The $\tau$-quantile loss is given by:
\begin{equation}
  \label{eq:quantile-loss}
  L(t;\tau) =
  \begin{cases}
    \tau t & t > 0\\
    -(1-\tau) t & \mbox{otherwise}
  \end{cases}.
\end{equation}

The $\tau$-quantile loss has the subgradient:
\begin{equation}
  \label{eq:quantile-loss-grad}
  \frac{\partial L(t;\tau)}{\partial t} =
  \begin{cases}
    \tau & t > 0\\
    -(1-\tau) & t < 0\\
    [\tau-1, \tau] & t = 0
  \end{cases}.
\end{equation}

Observe that the subgradient of $\tau$-quantile regression is one of two
constants, $\tau$ or $\tau-1$. This is called the pinball or tick loss.

\subsection{Stochastic Subgradient Descent}

Stochastic subgradient descent (SSGD).
Discuss convergence rate of different step size choices~(Figure~\ref{fig:sgd-convergence}).

Recall that we have the update rule
\[
t_{k+1} = t_k - \alpha_k g_k, \quad\mbox{where}\quad g_k\in\frac{\partial L(t)}{\partial t}
\]
To analyse convergence, we consider the error between the iterate $t_k$ at step $k$ and
the true solution $t^*$. This can be considered by considering the change that a single update
makes
\begin{align}
  \| t_{k+1} - t^* \|_2^2 &= \| t_{k} -\alpha_k g_k - t^* \|_2^2\\
  &= \| t_{k} - t^* \|_2^2 - 2\alpha_k \langle g_k, t_k - t^*\rangle + \alpha_k^2 \|g_k\|_2^2\\
  &\leqslant \| t_{k} - t^* \|_2^2 - 2\alpha_k ( L(t_k) - L(t^*) ) + \alpha_k^2 \|g_k\|_2^2.
\end{align}
The error after $T$ steps can be computed by a telescoping sum
\begin{equation}
  \| t_{T+1} - t^* \|_2^2 \leqslant \| t_1 - t^* \|_2^2
  - 2\sum_{k=1}^T \alpha_k ( L(t_k) - L(t^*) ) + \sum_{k=1}^T \alpha_k^2 \|g_k\|_2^2.
\end{equation}
This can be rearranged such that we have the losses on the left hand side
\[
2\sum_{k=1}^T \alpha_k ( L(t_k) - L(t^*) ) \leqslant \| t_1 - t^* \|_2^2
- \| t_{T+1} - t^* \|_2^2 + \sum_{k=1}^T \alpha_k^2 \|g_k\|_2^2.
\]

\begin{figure}[htbp]
\label{fig:sgd-convergence}
\includegraphics[width=\textwidth]{figures/sgd-convergence}
\caption{Rates of convergence for SSGD, from ICML 2010 tutorial}
\end{figure}



\newpage
\section{Frugal quantile estimator}
\label{sec:quantile}

Note that SSGD requires only one unit of memory to maintain the
current estimate of the quantile.
By applying SSGD with step size
$\frac{a}{\sqrt{k}}$ where $a$ is a constant and $k$ is the current
iteration, we get the following algorithm. Observe that we are currently
estimating each quantile independently, and potential improvements could be
obtained by using the fact that the quantiles are ordered.

\begin{listing}
  \begin{minted}{python}
  from math import sqrt

  def quantile_sgd(stream, quantile=0.75, init_step=100.):
      """Returns the quantile based on stochastic subgradient descent"""
      quantile_est = 0.
      for ix,val in enumerate(stream):
          step_size = (init_step/sqrt(float(ix+1)))
          if val > quantile_est:
              quantile_est += step_size*quantile
          else:
              quantile_est -= step_size*(1.-quantile)
      return quantile_est
  \end{minted}
  \label{lst:algorithm}
  \caption{Frugal quantile, implemented in python}
\end{listing}

Applying this to some data (30000 samples), and comparing it with the
randomised frugal streaming algorithm.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{figures/emp_quantiles_cauchy_1000_125}
\label{fig:compare-ecdf}
\caption{Comparing the estimated quantiles. SSGD is our proposed algorithm.
  Empirical is the CDF computed in a batch fashion. True is the quantiles of the generating distribution (\texttt{Cauchy (1000, 125)}).}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{figures/emp_quantiles_gauss_10_2}
\label{fig:compare-ecdf2}
\caption{Comparing the estimated quantiles. SSGD is our proposed
    algorithm. Empirical is the CDF computed in a batch fashion. True is
    the quantiles of the generating distribution (\texttt{Gaussian (-10, 2)}).}
\end{figure}

For estimating $q$ quantiles, we use the first $q$ items of the stream (in sorted order)
as the initial values of SGD. The scale of the variables are set of half of the maximum
and minimum of the sample.


\newpage
\section{Discussion of quantile estimator}

There are two challenges with the SGD convergence analysis:
\begin{enumerate}
  \item The range (scale) of the variables, captured by $\| t_1 - t^* \|_2^2$
  \item The gradients are constant for the tick loss
\end{enumerate}
The following two subsection could potentially be combined.

\subsection{Normalised Adaptive Gradient Descent}

Algorithm 2 in~\cite{ross13norol} is shown in~\ref{fig:nag}.

\begin{figure}[htbp]
\label{fig:nag}
\includegraphics[width=0.7\textwidth]{figures/nag}
\caption{Normalised Adaptive Gradient Descent}
\end{figure}



\subsection{Majorization-Maximization}

Consider $t_k$, the value of $t$ at step $k$. Let $g(t|t_k)$ be a real valued function of $t$
whose form depends on $t_k$. The function $g(t|t_k)$ is said to majorize~\cite{hunter04tutmma}
a function $f(t)$ at the point $t_k$ if
\begin{eqnarray*}
  g(t|t_k) \geqslant f(t)\mbox{~for all~}t\\
  g(t_k|t_k) = f(t_k).
\end{eqnarray*}
In particular, for the quantile loss (Equation~\ref{eq:quantile-loss}), at $t_k \neq 0$,
it is majorized by
\[
  M(t|t_k) = \frac{1}{4}\left(
    \frac{t^2}{|t_k|} + (4\tau - 2)t + |t_k|.
  \right)
\]
Setting the first derivative to zero gives the minimum point
\[
  t_{k+1} = \frac{(2\tau - 1) + \frac{x}{|x-t_k|}}{|x-t_k|}
\]
which is also the same update for Newton-Raphson since the function is quadratic.


\subsection{Further work?}

\begin{itemize}
\item Online Newton Method, Chapter 3 in Elad Hazan's PhD
 thesis~\url{http://www.cs.princeton.edu/~ehazan/papers/thesis.pdf}.
\item Bisection, see Nemirovski and Yudin 1983, and
  \url{http://people.orie.cornell.edu/shane/theses/ThesisRolfWaeber.pdf}.
\item Stochastic MM~\cite{mairal15incmmo}.
\end{itemize}


\newpage
\section{Ideas that did not work}

\subsection{Scale free quantile estimator}

For an estimate $f(x)$ and a given observation $y$, the general form for a scoring rule for a
$\tau$-quantile is
\[
  L_\tau(y, f(x)) = \tau s(y) + \mathbf{1}_{y\leqslant f(x)}(s(y)-s(f(x))) + h(y)
\]
Choosing
\[
s(y) = \log y \qquad\mbox{and}\qquad h(y) = -\tau \log y
\]
we get
\[
L_\tau(y, f(x)) = \tau \log\left(\frac{f(x)}{y}\right)
  + \mathbf{1}_{y\leqslant f(x)}\log\left(\frac{y}{f(x)}\right).
\]
This can be reexpressed by letting $t := \log\left(\frac{f(x)}{y}\right)$ to be exactly the same
as Equation~\ref{eq:quantile-loss}.
However, this is misleading, as the gradient is with respect to $f(x)$. We write it out in full
\begin{equation}
  \label{eq:scalefree-quantile-loss}
  L_\tau(y, f(x)) =
  \begin{cases}
    \tau (\log f(x) - \log y) & y > f(x)\\
    -(1-\tau) (\log f(x) - \log y) & \mbox{otherwise}
  \end{cases}.
\end{equation}

The $\tau$-quantile loss has the subgradient:
\begin{equation}
  \label{eq:scalefree-quantile-loss-grad}
  \frac{\partial L_\tau(y,f(x))}{\partial f(x)} =
  \begin{cases}
    \tau/f(x) & y > f(x)\\
    -(1-\tau)/f(x) & y < f(x)\\
    [\tau-1, \tau]/f(x) & y = f(x)
  \end{cases}.
\end{equation}

This is still not an ideal solution, as it only works for positive numbers. Furthermore,
there is more ``resolution'' near 0.


\subsection{Objection from Chris}
Here is some counter argument about $ S $:
Denote the CDF of $ f(x) $ by $ F(x) = \int_{-\infty}^x f(y) dy $. Then
for any choice of ordered $ x_1<x_2<\cdots<x_K$, the function $ S(x_1,\ldots,x_K) $ is \emph{exactly}
\[
  S(x_1,\ldots,x_K) = \sum_{k=0}^K \left( F(x_{k+1}) - F(x_k) \right)
                    = F(x_{K+1}) - F(x_0)
                    = F(1) - F(0) = \text{const}.
\]
because $ F(x_{k+1}) - F(x_k) $ represents the \emph{exact} area under
the density $f(x) $ for $ x_k \le x \le x_{k+1} $.

\subsection{We really want to use the pointwise estimate}

Consider the minimum integrated squared error~\cite{haerdle04nonsm}.


\subsubsection*{Acknowledgments}

\begin{itemize}
  \item Nishant Metha for discussions on multivariate medians
  \item Xinhua Zhang for convergence proofs of SGD
  \item Chrisfried Webers for relation to $\Sigma\Delta$ modulation
  \item Sebastian Nowozin for scale free quantile loss.
  \item Aditya Menon for discussions on isotonic regression
  \item Young Lee for background on Poisson processes
  \item Lexing Xie for background on dynamic programming
\end{itemize}
\subsubsection*{References}


\small{
{\bf Remember that this year you can use
a ninth page as long as it contains \emph{only} cited references.}

\bibliographystyle{alpha}
\bibliography{sgd}


\end{document}
