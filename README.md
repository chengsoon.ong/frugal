Frugal Histograms
=================

A low memory method for estimating the empirical cumulative
distribution function from data.

**Uses python 3**
