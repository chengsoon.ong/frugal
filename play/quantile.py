"""Qiang Ma, S. Muthukrishnan, Mark Sandler, "Frugal Streaming for Estimating Quantiles:One (or two) memory suffices"
arXiv:1407.1121"""

import numpy as np
from numpy.random import random

def quantile_sgd(stream, quantile=0.75, init_val=0., init_step=1.):
    """Returns the quantile based on stochastic subgradient descent"""
    quantile_est = init_val
    #step = init_step
    #quantile_est, step = doubling_sgd(stream, quantile, init_step)
    #print('Using step size = %f' % step)
    for ix,val in enumerate(stream):
        if val > quantile_est:
            quantile_est += (init_step/np.sqrt(float(ix+1)))*quantile
        else:
            quantile_est -= (init_step/np.sqrt(float(ix+1)))*(1.-quantile)
    return quantile_est

def quantile_min(stream, quantile=0.75, init_val=1., init_step=1.):
    """Returns a quantile based on stochastic subgradient descent.
    Step size heuristic: minimum difference between estimate and observation.
    """
    quantile_est = init_val
    for ix,val in enumerate(stream):
        min_gap = abs(val - quantile_est)
        if val > quantile_est:
            quantile_est += (min_gap/np.sqrt(float(ix+1)))*quantile
        else:
            quantile_est -= (min_gap/np.sqrt(float(ix+1)))*(1.-quantile)
    return quantile_est

def doubling_sgd(stream, quantile, init_step):
    """Step in direction of error with SGD with a step size that doubles,
    until a change of direction is needed.
    """
    quantile_est = 0.
    step = init_step
    increasing = stream[0] > quantile_est
    for ix,val in enumerate(stream):
        if val > quantile_est:
            quantile_est += step*quantile
            if increasing:
                step *= 2.
            else:
                return quantile_est, step
        else:
            quantile_est -= step*(1.-quantile)
            if not increasing:
                step *= 2.
            else:
                return quantile_est, step

def quantile_sgd_scalefree(stream, quantile=0.75, init_val=1., init_step=1.):
    """Returns the quantile based on stochastic subgradient descent"""
    quantile_est = 1.
    for ix,val in enumerate(stream):
        if val > quantile_est:
            quantile_est += (init_step/np.sqrt(float(ix+1)))*quantile/abs(quantile_est)
        else:
            quantile_est -= (init_step/np.sqrt(float(ix+1)))*(1.-quantile)/abs(quantile_est)
    return quantile_est

def median(stream):
    """Return the median"""
    median_est = 0
    for val in stream:
        if val > median_est:
            median_est += 1
        elif val < median_est:
            median_est -= 1
    return median_est

def quantile1U(stream, quantile=0.75):
    """Return the quantile.
    Uses only one bit of memory.
    """
    quantile_est = 0
    for val in stream:
        r = random()
        if val > quantile_est and r > 1 - quantile:
            quantile_est += 1
        elif val < quantile_est and r > quantile:
            quantile_est -= 1
    return quantile_est

def quantile2U(stream, m=0, quantile=0.75, f=1):
    """Return the quantile.
    Uses only two bits of memory.
    """
    step = 1
    sign = 1
    for item in stream:
        r = random()
        if item > m and r > 1 - quantile:
            # Increment the step size if and only if the estimate keeps moving in
            # the same direction. Step size is incremented by the result of applying
            # the specified step function to the previous step size.
            step += f if sign > 0 else -1 * f
            # Increment the estimate by step size if step is positive. Otherwise,
            # increment the step size by one.
            m += step if step > 0 else 1
            # Mark that the estimate increased this step
            sign = 1
            # If the estimate overshot the item in the stream, pull the estimate back
            # and re-adjust the step size.
            if m > item:
                step += (item - m)
                m = item
        # If the item is less than the stream, follow all of the same steps as
        # above, with signs reversed.
        elif item < m and r > quantile:
            step += f if sign < 0 else -1 * f
            m -= step if step > 0 else 1
            sign = -1
            if m < item:
                step += (m - item)
                m = item
        # Damp down the step size to avoid oscillation.
        if (m - item) * sign < 0 and step > 1:
            step = 1
    return m
