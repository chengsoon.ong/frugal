"""Bayesian Blocks algorithm for events,
Simplified from implementation in astroML
See: https://github.com/astropy/astropy/blob/master/astropy/stats/bayesian_blocks.py

Dynamic programming algorithm for finding the optimal adaptive-width histogram.
Based on Scargle et al 2012 [1]_

References
----------
.. [1] http://adsabs.harvard.edu/abs/2012arXiv1207.5578S
"""

import numpy as np

def fitness(N_k, T_k):
    """Fitness for events.
    eq. 19 from Scargle 2012
    """
    return N_k * (np.log(N_k) - np.log(T_k))

def p0_prior(N, p0=0.01):
    """
    Empirical prior, parametrized by the false alarm probability ``p0``
    See  eq. 21 in Scargle (2012)
    Note that there was an error in this equation in the original Scargle
    paper (the "log" was missing). The following corrected form is taken
    from http://arxiv.org/abs/1304.2818
    """
    return 4 - np.log(73.53 * p0 * (N ** -0.478))

def gamma_prior(N, Ntot, gamma):
    """
    Basic prior, parametrized by slope gamma.
    See eq. 3 in Scargle (2012)
    """
    if gamma >= 1:
        return 0
    else:
        return -(np.log(1. - gamma)
                - np.log(1. - gamma ** (Ntot + 1.))
                + N * np.log(gamma))

def log_likelihood(N_k, T_k):
    """Log likelihood with maximum likelihood estimate of intensity"""
    return fitness(N_k, T_k) - N_k

def unique_events(events):
    """Return an array of unique event locations and their counts"""
    events = np.asarray(events, dtype=float)

    # find unique values of events
    assert events.ndim == 1
    unq_ev, unq_ind, unq_inv = np.unique(events, return_index=True,
                                        return_inverse=True)
    if len(unq_ev) == len(events):
        ev_count = np.ones_like(events)
    else:
        ev_count = np.bincount(unq_inv)
    events = unq_ev
    return events, ev_count

def adaptive_histogram(events, prior=0.999):
    """Construct a histogram with uneven bin widths.
    Returns the bins
    """
    events, ev_count = unique_events(events)
    N = events.size

    # create length-(N + 1) array of cell edges
    edges = np.concatenate([events[:1], 0.5 * (events[1:] + events[:-1]), events[-1:]])
    block_length = events[-1] - edges
    # arrays to store the best configuration
    best = np.zeros(N, dtype=float)
    last = np.zeros(N, dtype=int)

    #-----------------------------------------------------------------
    # Start with first data cell; add one cell at each iteration
    # Compute fit_vec : fitness of putative last block (end at R)
    #-----------------------------------------------------------------
    for R in range(N):
        # T_k: width/duration of each block
        # N_k: number of elements in each block
        T_k = block_length[:R + 1] - block_length[R + 1]
        N_k = np.cumsum(ev_count[:R + 1][::-1])[::-1]

        # evaluate fitness function
        fit_vec = fitness(N_k, T_k)
        fit_vec -= gamma_prior(R+1, N, prior)
        fit_vec[1:] += best[:R]

        idx_max = np.argmax(fit_vec)
        last[R] = idx_max
        best[R] = fit_vec[idx_max]

    #-----------------------------------------------------------------
    # Now find changepoints by iteratively peeling off the last block
    #-----------------------------------------------------------------
    change_points = np.zeros(N, dtype=int)
    idx_cp = N
    ind = N
    while True:
        idx_cp -= 1
        change_points[idx_cp] = ind
        if ind == 0:
            break
        ind = last[ind - 1]
    change_points = change_points[idx_cp:]

    return edges[change_points]


def score_vec(bins, events, score='density'):
    """Returns the log likelihood defined by the bin edges.
    Score can be one of {'density', 'intensity', 'loglike', 'fitness'}
    """
    num_bins = len(bins) - 1
    T_k = bins[1:]-bins[:-1]
    events, ev_count = unique_events(events)
    N_k = np.zeros(num_bins)
    for R in range(num_bins):
        idx_ev = np.logical_and(events > bins[R], events < bins[R+1])
        N_k[R] = np.sum(ev_count[idx_ev])

    if score == 'density':
        fit_vec = (N_k/T_k)/float(np.sum(ev_count))
    elif score == 'intensity':
        fit_vec = N_k/T_k
    elif score == 'loglike':
        fit_vec = log_likelihood(N_k, T_k)
    elif score == 'fitness':
        fit_vec = fitness(N_k, T_k)
    else:
        raise NotImplementedError
    assert num_bins == len(fit_vec)
    return fit_vec
