"""Run a set of quantile estimators in parallel to estimate a histogram."""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
from numpy.random import random
from quantile import quantile1U, quantile_sgd, quantile_min
from scipy.stats.mstats import mquantiles

def histogram(stream):
    """Returns the histogram of the stream"""
    quant_vec, count = emp_cdf(stream)
    bin_width = largest_step(quant_vec)
    bins = np.arange(quant_vec[0], quant_vec[-1], bin_width)
    props = quantile2epdf(quant_vec, bins)
    show_hist(count*props, bins)

def emp_cdf_independent(stream, bins=100, quant_func=quantile_sgd):
    """Estimate the value at each quantile independently,
     and return the empirical CDF and the total number of items observed"""
    quant_vec = np.zeros(bins-1)
    count = len(stream)
    init_vec = stream[:bins].copy()
    stream = stream[bins:]
    init_vec.sort()
    init_step = 0.5*(init_vec[-1]-init_vec[0])
    for ix in range(bins-1):
        q = float(ix+1)/float(bins)
        quant_vec[ix] = quant_func(stream, quantile=q, init_val=init_vec[ix], init_step=init_step)

    return quant_vec, count

def emp_cdf(stream, bins=100):
    """Estimate the value at each quantile,
     and return the empirical CDF and the total number of items observed"""
    quantiles = np.arange(1./bins,1,1./bins)
    quant_vec = np.arange(1,bins)
    for val in stream:
        r = random()
        bigger_val = quant_vec < val
        smaller_val = quant_vec > val
        bigger_top = (1. - quantiles) <= r
        bigger_bot = quantiles <= r
        to_increase = np.logical_and(bigger_val, bigger_top)
        to_decrease = np.logical_and(smaller_val, bigger_bot)
        quant_vec[to_increase] += 1
        quant_vec[to_decrease] -= 1
    return quant_vec, len(stream)

def plot_ecdf(quantiles, names, q_vals):
    for ix,q in enumerate(quantiles):
        plt.step(q, q_vals, label=names[ix])
    plt.legend(loc='lower right')
    plt.xlabel('estimated value')
    plt.ylabel('quantile')
    plt.ylim([0,1.1])
    plt.savefig('emp_cdf.pdf')

def plot_quantiles(quantiles, names, q_vals):
    cNorm  = colors.Normalize(vmin=0, vmax=1)
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=plt.get_cmap('winter') )
    style = ['-.', '--', '-']

    fig = plt.figure(figsize=(8,1.5))
    ax = fig.add_subplot(111)
    for ix_meth,quant in enumerate(quantiles):
        for idx,q in enumerate(quant):
            colorVal = scalarMap.to_rgba(q_vals[idx])
            if idx == 0:
                ax.plot([q, q], [0, 1], style[ix_meth], label=names[ix_meth], color=colorVal)
            else:
                ax.plot([q, q], [0, 1], style[ix_meth], color=colorVal)
    ax.set_xlabel('estimated value')
    ax.set_yticks([])
    ax.set_ylim([-0.1,1.1])
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.5,
                    box.width, box.height * 0.5])

    # Put a legend below current axis
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.3),
                fancybox=True, shadow=True, ncol=len(quantiles))
    return fig

def sim_and_plot(rv, num_q=10):
    stream = rv.rvs(size=30000)
    qv_sgd, c = emp_cdf_independent(stream, num_q)
    #qv, c = emp_cdf(stream, num_q)

    quantiles = np.arange(1./num_q,1,1./num_q)
    qv_emp = mquantiles(stream, prob=quantiles)
    qv_true = rv.ppf(quantiles)

    print(c)
    np.set_printoptions(precision=2)
    print('Error 100*(stream - true)/true')
    print(100.*(qv_sgd-qv_true)/qv_true)
    #diff = qv[1:]-qv[:-1]
    #assert(np.all(diff >= 0))
    fig = plot_quantiles([qv_sgd, qv_emp, qv_true],
                ['SSGD', 'empirical', 'true'],
                quantiles)
    return fig
