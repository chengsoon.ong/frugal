from scipy.stats import cauchy
from scipy.stats.mstats import mquantiles
from quantile import median, quantile1U, quantile2U

rv = cauchy(loc=10000, scale=1250)
stream = rv.rvs(size=30000)
med = median(stream)
q75_1U = quantile1U(stream)
q75_2U = quantile2U(stream)

print(mquantiles(stream, prob=[0.5, 0.75]))
print(med, q75_1U, q75_2U)
