import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
from scipy.stats import cauchy, norm, expon
from histogram import sim_and_plot, emp_cdf_independent
from adaptive_histogram import adaptive_histogram, score_vec
from scipy.stats.mstats import mquantiles

def quantile_demo():
    """Simulate some data and show the effect of the low memory quantile estimator"""
    rv = cauchy(loc=1000, scale=125)
    fig = sim_and_plot(rv)
    fig.savefig('emp_quantiles_cauchy_1000_125.pdf')
    rv = norm(loc=-10, scale=2)
    fig = sim_and_plot(rv)
    fig.savefig('emp_quantiles_gauss_10_2.pdf')

def _exp_gauss():
    """Generate some data and compute the adaptive bins"""
    np.random.RandomState(0)
    rv_broad = norm(loc=10, scale=4)
    rv_narrow = expon(loc=2, scale=2)
    stream = np.concatenate([rv_broad.rvs(size=2500), -rv_narrow.rvs(size=2500)])
    bins = adaptive_histogram(stream)
    return bins, stream

def _plot_true_density(ax):
    rv_broad = norm(loc=10, scale=4)
    rv_narrow = expon(loc=2, scale=2)
    lower = min(rv_broad.ppf(0.001), -rv_narrow.ppf(0.001))
    upper = max(rv_broad.ppf(0.999), -rv_narrow.ppf(0.999))
    x = np.linspace(lower, upper, 200)
    ax.plot(-x, 0.5*rv_narrow.pdf(x), 'g-', linewidth=2)
    ax.plot(x, 0.5*rv_broad.pdf(x), 'g-', linewidth=2)
    ax.set_ylabel('density', color='g')
    for tl in ax.get_yticklabels():
        tl.set_color('g')

def bb_demo():
    """Simulate some data and show the Bayesian Blocks algorithm for adaptive histograms."""
    bins, stream = _exp_gauss()
    score = score_vec(bins, stream)
    fig = plt.figure(figsize=(8,6))
    ax1 = fig.add_subplot(111)
    n, bins, patches = ax1.hist(stream, bins)
    plt.setp(patches, 'facecolor', 'b')
    ax1.set_ylabel('number of events', color='b')
    for tl in ax1.get_yticklabels():
        tl.set_color('b')
    autolabel(ax1, patches, score)

    ax2 = ax1.twinx()
    _plot_true_density(ax2)
    fig.savefig('emp_histogram_gauss_mixture.pdf')

def autolabel(ax, rects, texts):
    # attach some text labels
    for ix,rect in enumerate(rects):
        height = rect.get_height()
        ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%1.3f'%texts[ix],
                ha='center', va='bottom', rotation=80, fontsize=9)

def hist_demo():
    bins, stream = _exp_gauss()
    fig = plt.figure(figsize=(16,9))

    ax1 = fig.add_subplot(221)
    score = score_vec(bins, stream, 'intensity')
    plot_bars(ax1, bins, score)
    ax1.set_title('piecewise constant intensity')
    ax1.set_ylabel('$\lambda(x) = N \cdot p(x)$')
    ax1.set_xlim([-20,25])
    ax1.set_ylim([0, 1500])

    ax2 = ax1.twinx()
    _plot_true_density(ax2)
    ax2.set_ylabel('true density')
    ax2.set_xlim([-20,25])
    ax2.set_ylim([0, 15/50.])

    ax3 = fig.add_subplot(222)
    score = score_vec(bins, stream, 'loglike')
    plot_bars(ax3, bins, score)
    ax3.set_title('log likelihood $\mathcal{L}(\hat\lambda)$')
    ax3.set_xlim([-20,25])

    ax4 = fig.add_subplot(223)
    n, b, patches = ax4.hist(stream, bins)
    plt.setp(patches, 'facecolor', 'b')
    ax4.set_title('adaptive histogram (Scargle et. al. 2013)')
    ax4.set_ylabel('number of events')
    ax4.set_xlim([-20,25])

    ax5 = fig.add_subplot(224)
    n, b, patches = ax5.hist(stream, 25)
    plt.setp(patches, 'facecolor', 'b')
    ax5.set_title('histogram, uniform bins')
    ax5.set_ylabel('number of events')
    ax5.set_xlim([-20,25])

    fig.savefig('adapt_histogram_gauss_mixture.pdf', bbox_inches='tight')


def plot_bars(ax, edges, heights):
    """Plot a bar chart which looks like a histogram.
    edges define the (edges-1) bars, each of which is height tall.
    """
    assert len(edges)-1 == len(heights)
    for ix,h in enumerate(heights):
        ax.add_patch(patches.Rectangle((edges[ix],0), edges[ix+1]-edges[ix], h))
        ax.text(0.5*(edges[ix+1]+edges[ix]), 1.05*h, '%1.3f'%h,
                ha='center', va='bottom', rotation=80, fontsize=9)
    ax.set_xlim([edges[0], edges[-1]])
    ax.set_ylim([0, np.max(heights)])

def hist_vs_quantile():
    """Check that adaptive histogram bin edges are not quantiles"""
    rv = norm(loc=10, scale=2)
    stream = rv.rvs(size=10000)
    bin_edges = adaptive_histogram(stream)
    num_q = len(bin_edges) - 1
    qv_sgd, c = emp_cdf_independent(stream, num_q)

    quantiles = np.arange(1./num_q,1,1./num_q)
    qv_emp = mquantiles(stream, prob=quantiles)
    qv_true = rv.ppf(quantiles)

    print(c)
    np.set_printoptions(precision=3)
    print('Adaptive histogram bin edges')
    print(bin_edges)
    print('Streaming quantiles')
    print(qv_sgd)
    print('Sample quantiles')
    print(qv_emp)
    print('True quantiles')
    print(qv_true)

def plot_hist_vs_quantile():
    """Visualise the histogram based on piecewise constant Poisson intensity.
    Compare optimal adaptation and quantiles.
    """
    rv = norm(loc=10, scale=2)
    stream = rv.rvs(size=10000)
    bin_edges = adaptive_histogram(stream)
    num_q = len(bin_edges) - 1
    quantiles = np.arange(1./num_q,1,1./num_q)
    qv_true = rv.ppf(quantiles)
    print('Quantiles')
    print(qv_true)

    fig = plt.figure(figsize=(16,5))

    ax1 = fig.add_subplot(121)
    score = score_vec(bin_edges, stream, 'intensity')
    plot_bars(ax1, bin_edges, score)
    ax1.set_title('Adaptive histogram bin edges')
    ax1.set_ylabel('$\lambda(x) = N \cdot p(x)$')
    ax1.set_xlim([0,20])
    ax1.set_ylim([0, 2500])

    ax3 = fig.add_subplot(122)
    score = score_vec(qv_true, stream, 'intensity')
    plot_bars(ax3, qv_true, score)
    ax3.set_title('Quantile bin edges')
    ax3.set_ylabel('$\lambda(x) = N \cdot p(x)$')
    ax3.set_xlim([0,20])
    ax3.set_ylim([0, 2500])

    fig.savefig('hist_vs_quantile.pdf', bbox_inches='tight')


if __name__ == '__main__':
    plot_hist_vs_quantile()
    #hist_vs_quantile()
    #quantile_demo()
    #bb_demo()
    #hist_demo()
