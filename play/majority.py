"""MJRTY - A Fast Majority Vote Algorithm, with R.S. Boyer. In R.S. Boyer (ed.), Automated Reasoning: Essays in Honor of Woody Bledsoe, Automated Reasoning Series, Kluwer Academic Publishers, Dordrecht, The Netherlands, 1991, pp. 105-117."""

def mjrty(stream):
    """Assume that a majority element exists.
    Returns the majority element.
    """
    count = 0
    majority = ""

    for val in stream:
        if count == 0:
            majority = val
            count = 1
        elif val == majority:
            count += 1
        else:
            count -= 1
    return majority
